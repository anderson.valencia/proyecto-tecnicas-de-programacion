package dao.imp;

import dao.EnvioDAO;
import model.Empleado;
import model.Envio;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.nio.file.StandardOpenOption.APPEND;

public class EnvioDAONIO implements EnvioDAO {

    private final static String NOMBRE_ARCHIVO = "envios";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static Integer LONGITUD_REGISTRO = 126;
    private final static Integer LONGITUD_ID = 10;
    private final static Integer LONGITUD_DIRECCION = 80;
    private final static Integer LONGITUD_PRECIO = 10;
    private final static Integer LONGITUD_FECHA = 10;
    private final static Integer LONGITUD_CATEGORIA = 15;
    private final static Integer LONGITUD_ENTREGADO = 1; // Se almacenará 1 si es true, y 0 si es false.

    public EnvioDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void añadirEnvio(Envio envio) {

        // Se obtiene la representación en String del empleado.
        String registro = parseEnvioString(envio);
        // Se extraen los bytes del registro.
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer.
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    @Override
    public void limpiar() throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(ARCHIVO);
        writer.write("");
        writer.flush();
    }

    @Override
    public List<Envio> obtenerListaEnvios() {

        List<Envio> listaEnvios = new ArrayList<>();
        // Abrimos un Canal de Bytes buscable (SeekableByteChannel) para leer los datos.
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            // Abrimos un ByteBuffer para capturar datos. En este caso, captura 50 caracteres.
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            // Lee paquetes de Bytes hasta que llegue al final del archivo.
            while (sbc.read(buffer) > 0) {
                // Ubica el apuntador del buffer en la posición inicial.
                buffer.rewind();
                // Decodifica los bytes usando el juego de caracteres por defecto del sistema operativo.
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Envio envio = parseBufferEnvio(registro);
                listaEnvios.add(envio);
                // Prepara el buffer para leer Bytes del disco de nuevo.
                buffer.flip();

            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        return listaEnvios;
    }

    private Envio parseBufferEnvio(CharBuffer registro) {

        Envio envio = new Envio();

        String id = registro.subSequence(0, LONGITUD_ID).toString().trim();
        envio.setId(id);
        registro.position(LONGITUD_ID);
        registro = registro.slice();

        String direccion = registro.subSequence(0, LONGITUD_DIRECCION).toString().trim();
        envio.setDireccion(direccion);
        registro.position(LONGITUD_DIRECCION);
        registro = registro.slice();

        String precio = registro.subSequence(0, LONGITUD_PRECIO).toString().trim();
        envio.setPrecio(precio);
        registro.position(LONGITUD_PRECIO);
        registro = registro.slice();

        String fecha = registro.subSequence(0, LONGITUD_FECHA).toString().trim();

        LocalDate localDate = LocalDate.parse(fecha);

        envio.setFecha(localDate);
        registro.position(LONGITUD_PRECIO);
        registro = registro.slice();

        String entregado = registro.subSequence(0, LONGITUD_ENTREGADO).toString().trim();
        if(entregado.equals("1")){
            envio.setEntregado(true);
        } else {
            envio.setEntregado(false);
        }
        registro.position(LONGITUD_ENTREGADO);
        registro = registro.slice();

        String categoria = registro.subSequence(0, LONGITUD_CATEGORIA).toString().trim();
        envio.setCategoria(categoria);
        registro.position(LONGITUD_CATEGORIA);
        registro = registro.slice();

        return envio;

    }

    private String parseEnvioString(Envio envio) {

        String estadoEntrega;
        if(envio.isEntregado()){
            estadoEntrega = "1";
        } else {
            estadoEntrega = "0";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(completarCampo(envio.getId(),LONGITUD_ID)).append(completarCampo(envio.getDireccion(),LONGITUD_DIRECCION))
                .append(completarCampo(envio.getPrecio(),LONGITUD_PRECIO)).append(completarCampo(envio.getFecha().toString(),LONGITUD_FECHA))
                .append(completarCampo(estadoEntrega,LONGITUD_ENTREGADO)).append(completarCampo(envio.getCategoria(),LONGITUD_CATEGORIA));
        return sb.toString();


    }

    private String completarCampo(String valor, Integer longitudCampo) {
        return String.format("%1$" + longitudCampo + "s", valor);
    }

}
