package dao.imp;

import dao.EmpleadoDAO;
import model.Empleado;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.invoke.LambdaConversionException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;

public class EmpleadoDAONIO implements EmpleadoDAO {

    // Crear un archivo.
    private final static String NOMBRE_ARCHIVO = "empleados";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static Integer LONGITUD_REGISTRO = 60;
    private final static Integer LONGITUD_NOMBRE = 20;
    private final static Integer LONGITUD_ID = 10;
    private final static Integer LONGITUD_CARGO = 30;

    public EmpleadoDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Poner los datos del empleado en el archivo creado.
    @Override
    public void registrarEmpleado(Empleado empleado) {

        // Se obtiene la representación en String del empleado.
        String registro = parseEmpleadoString(empleado);
        // Se extraen los bytes del registro.
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer.
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }


    @Override
    public void limpiar() throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(ARCHIVO);
        writer.write("");
        writer.flush();
    }

    // Busca un empleado por su id. En caso de no encontrar uno cuyo id coincida con el ingresado, devuelve null.
    @Override
    public Empleado consultarPorId(String id) {

        List<Empleado> listEmpleados = obtenerListaEmpleados();
        for (Empleado empleado : listEmpleados) {
            if (empleado.getId().equals(id)) {
                return empleado;
            }
        }
        return null;
    }

    // Devuelve una lista con todos los empleados.
    @Override
    public List<Empleado> obtenerListaEmpleados() {

        List<Empleado> listEmpleados = new ArrayList<>();
        // Abrimos un Canal de Bytes buscable (SeekableByteChannel) para leer los datos.
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            // Abrimos un ByteBuffer para capturar datos. En este caso, captura 50 caracteres.
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            // Lee paquetes de Bytes hasta que llegue al final del archivo.
            while (sbc.read(buffer) > 0) {
                // Ubica el apuntador del buffer en la posición inicial.
                buffer.rewind();
                // Decodifica los bytes usando el juego de caracteres por defecto del sistema operativo.
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Empleado empleado = parseBufferEmpleado(registro);
                listEmpleados.add(empleado);
                // Prepara el buffer para leer Bytes del disco de nuevo.
                buffer.flip();

            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return listEmpleados;
    }



    private Empleado parseBufferEmpleado(CharBuffer registro) {
        Empleado empleado = new Empleado();

        String nombre = registro.subSequence(0, LONGITUD_NOMBRE).toString().trim();
        empleado.setNombre(nombre);
        registro.position(LONGITUD_NOMBRE);
        registro = registro.slice();

        String id = registro.subSequence(0, LONGITUD_ID).toString().trim();
        empleado.setId(id);
        registro.position(LONGITUD_ID);
        registro = registro.slice();

        String cargo = registro.subSequence(0, LONGITUD_CARGO).toString().trim();
        empleado.setCargo(cargo);
        registro.position(LONGITUD_CARGO);
        registro = registro.slice();

        return empleado;
    }

    private String parseEmpleadoString(Empleado empleado) {
            StringBuilder sb = new StringBuilder();
            sb.append(completarCampo(empleado.getNombre(), LONGITUD_NOMBRE))
                    .append(completarCampo(empleado.getId(), LONGITUD_ID))
                    .append(completarCampo(empleado.getCargo(), LONGITUD_CARGO));
            return sb.toString();
    }

    private String completarCampo(String valor, Integer longitudCampo) {
        return String.format("%1$" + longitudCampo + "s", valor);
    }

}
