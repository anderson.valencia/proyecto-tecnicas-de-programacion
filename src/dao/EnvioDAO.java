package dao;

import model.Envio;

import java.io.IOException;
import java.util.List;

public interface EnvioDAO {

    void añadirEnvio(Envio envio);

    List<Envio> obtenerListaEnvios();

    void limpiar() throws IOException;


}
