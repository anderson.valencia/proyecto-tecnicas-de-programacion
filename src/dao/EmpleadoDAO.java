package dao;

import model.Empleado;

import java.io.IOException;
import java.util.List;

public interface EmpleadoDAO {

    void registrarEmpleado(Empleado empleado);

    Empleado consultarPorId(String id);

    List<Empleado> obtenerListaEmpleados();

    void limpiar() throws IOException;

}
