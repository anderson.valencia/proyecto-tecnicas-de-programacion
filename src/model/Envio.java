package model;

import java.time.LocalDate;

public class Envio {

    private String id;
    private String direccion;
    private String precio;
    private LocalDate fecha;
    private String categoria;
    private boolean entregado;

    public Envio(){

    }

    public Envio (String id, String direccion, String precio, LocalDate fecha, String categoria){
        this.id = id;
        this.direccion = direccion;
        this.precio = precio;
        this.fecha = fecha;
        this.categoria = categoria;
        this.entregado = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public boolean isEntregado() {
        return entregado;
    }

    public void setEntregado(boolean entregado) {
        this.entregado = entregado;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Envio{" +
                "id='" + id + '\'' +
                ", direccion='" + direccion + '\'' +
                ", precio='" + precio + '\'' +
                ", fecha=" + fecha +
                ", categoria='" + categoria + '\'' +
                ", entregado=" + entregado +
                '}';
    }
}
