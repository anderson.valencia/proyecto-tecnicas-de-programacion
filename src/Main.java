import controller.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import javax.swing.*;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader =  new FXMLLoader(getClass().getResource("view/login.fxml"));
        Parent root = loader.load();
        LoginController controller = loader.getController();
        controller.setStage(primaryStage);
        primaryStage.setTitle("Sistema de Envios");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();


    }


    public static void main(String[] args) {

        JOptionPane.showMessageDialog(null,"Querido Usuario \n\n El programa presenta ciertos errores cuando se ingresan datos usandos tildes (´), por favor, sea bueno con el programa, no use tildes. ");

        launch(args);
    }
}
