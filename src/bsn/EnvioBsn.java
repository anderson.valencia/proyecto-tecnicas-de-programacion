package bsn;

import dao.EnvioDAO;
import dao.imp.EnvioDAONIO;
import model.Envio;

import java.io.IOException;
import java.util.List;

public class EnvioBsn {

    EnvioDAO envioDAO;

    public EnvioBsn(){
        envioDAO = new EnvioDAONIO();
    }

    public void añadirEnvio(Envio envio){
        envioDAO.añadirEnvio(envio);
    }

    public List<Envio> obtenerListaEnvios(){
        return envioDAO.obtenerListaEnvios();
    }

    public void limpiar() throws IOException {
        envioDAO.limpiar();
    }

}
