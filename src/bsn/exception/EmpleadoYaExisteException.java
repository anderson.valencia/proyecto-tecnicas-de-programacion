package bsn.exception;

public class EmpleadoYaExisteException extends Exception {

    public EmpleadoYaExisteException(){
        super("Ya existe un empleado con el mismo id ingresado.");
    }

}
