package bsn;

import bsn.exception.EmpleadoYaExisteException;
import dao.EmpleadoDAO;
import dao.imp.EmpleadoDAONIO;
import model.Empleado;

import java.io.IOException;
import java.util.List;

public class EmpleadoBsn {

    EmpleadoDAO empleadoDAONIO;

    public EmpleadoBsn(){
        empleadoDAONIO = new EmpleadoDAONIO();
    }

    public void registrarEmpleado(Empleado empleado) throws EmpleadoYaExisteException {
        Empleado empleadoPrevio = this.empleadoDAONIO.consultarPorId(empleado.getId());
        if(empleadoPrevio == null){
            empleadoDAONIO.registrarEmpleado(empleado);
        } else {
            throw new EmpleadoYaExisteException();
        }
    }

    public List<Empleado> obtenerListaEmpleados(){
        return empleadoDAONIO.obtenerListaEmpleados();
    }

    public void limpiar() throws IOException {
        empleadoDAONIO.limpiar();
    }

}
