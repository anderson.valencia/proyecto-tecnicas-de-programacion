package controller;

import bsn.EnvioBsn;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import model.Empleado;
import model.Envio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GestionarEnviosEmpleadoController {

    static Empleado empleado;

    @FXML
    ChoiceBox cBoxEnvios;

    @FXML
    Label txtEmpleado;

    @FXML
    Button btnEntregado, btnNoEntregado;

    EnvioBsn envioBsn;

    public GestionarEnviosEmpleadoController() {
        envioBsn = new EnvioBsn();
    }

    public void setEmpleado(Empleado empleado){
        this.empleado = empleado;
    }

    @FXML
    public void initialize() {
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        List<Envio> listaEnviosSeleccionado = new ArrayList<>();
        txtEmpleado.setText(empleado.getNombre());

        for(Envio envio: listaEnvios){
            if(envio.getId().equals(empleado.getId())){
                listaEnviosSeleccionado.add(envio);
            }
        }
        actualizarEnvios();

    }

    public void actualizarEnvios(){

        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        String idSeleccionado =  empleado.getId();;


        List<Envio> listaEnviosEmpleadoSeleccionado = new ArrayList<>();

        for(Envio envio: listaEnvios){
            if(envio.getId().equals(idSeleccionado)){
                listaEnviosEmpleadoSeleccionado.add(envio);
            }
        }

        for(Envio envio: listaEnviosEmpleadoSeleccionado){
            cBoxEnvios.getItems().add(envio.toString());
        }
    }

    public void btnEntregado_action() throws IOException {
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        String envioSeleccionado = cBoxEnvios.getValue().toString();

        for(Envio envio: listaEnvios){
            if (envio.toString().equals(envioSeleccionado)) {
                envio.setEntregado(true);
            }
        }

        envioBsn.limpiar();
        for(Envio envio: listaEnvios){
            envioBsn.añadirEnvio(envio);
        }
    }

    public void btnNoEntregado_action() throws IOException {
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        String envioSeleccionado = cBoxEnvios.getValue().toString();

        for(Envio envio: listaEnvios){
            if (envio.toString().equals(envioSeleccionado)) {
                envio.setEntregado(false);
            }
        }

        envioBsn.limpiar();
        for(Envio envio: listaEnvios){
            envioBsn.añadirEnvio(envio);
        }
    }

}
