package controller;

import bsn.EmpleadoBsn;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import model.Empleado;

import java.awt.*;
import java.io.IOException;
import java.util.List;

public class LoginController {

    @FXML
    javafx.scene.control.TextField txtUsuarioLogin;

    @FXML
    PasswordField txtPasswordLogin;

    @FXML
    public void initialize(){

    }

    private static Stage stage;
    EmpleadoBsn empleadoBsn;
    ContenedorEmpleadoController contenedorEmpleadoController;
    ContenedorPrincipalController contenedorPrincipalController;

    public LoginController(){
        empleadoBsn = new EmpleadoBsn();
        contenedorEmpleadoController = new ContenedorEmpleadoController();
        contenedorPrincipalController = new ContenedorPrincipalController();
    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public void btnIngresar_action() throws IOException {
        String usuarioIngresado = txtUsuarioLogin.getText();
        String passwordIngresada = txtPasswordLogin.getText();

        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();

        String USUARIO_GERENTE = "gerente";
        String PASSWORD_GERENTE = "123";

        if(usuarioIngresado.equals(USUARIO_GERENTE) && passwordIngresada.equals(PASSWORD_GERENTE)){
            Parent ventanaInicial = FXMLLoader.load(getClass().getResource("../view/contenedor-principal.fxml"));
            stage.setTitle("Sistema de envios");
            stage.setScene(new Scene(ventanaInicial,300,275));
            contenedorPrincipalController.setStage(stage);
            return;
        }

        for(Empleado empleado: listaEmpleados){
            if(empleado.getNombre().equals(usuarioIngresado) && empleado.getId().equals(passwordIngresada)){
                Parent ventanaInicial = FXMLLoader.load(getClass().getResource("../view/contenedor-empleado.fxml"));
                stage.setTitle("Sistema de envios");
                stage.setScene(new Scene(ventanaInicial,300,275));
                System.out.println(empleado.toString());
                contenedorEmpleadoController.setEmpleado(empleado);
                contenedorEmpleadoController.setStage(stage);
                return;
            }

        }

        ventanaTextoError("Usuario o contraseña incorrecto.");

    }

    // Abrir una ventana información que de un mensaje determinado.
    public void ventanaTextoInformation(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error al ingresar.");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    // Abrir una ventana de Error que de un mensaje determinado.
    public void ventanaTextoError(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error al ingresar");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();

    }

}
