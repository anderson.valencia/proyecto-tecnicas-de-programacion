package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ContenedorPrincipalController {

    @FXML
    BorderPane contenedorPrincipal;

    static Stage stage;

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public void menuItemCerrar_action(){
        System.exit(0);
    }

    public void menuItemRegistrarEmpleado_action() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/registrar-empleado.fxml"));
        contenedorPrincipal.setCenter(root);
    }

    public void menuItemAñadirEnvio_action() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/añadir-envio.fxml"));
        contenedorPrincipal.setCenter(root);
    }

    public void menuItemVerEnvios_action() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/ver-envios.fxml"));
        contenedorPrincipal.setCenter(root);
    }

    public void menuItemVerEmpleados_action() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/ver-empleados.fxml"));
        contenedorPrincipal.setCenter(root);
    }

    public void menuItemGestionarEnvios_action() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/gestionar-envios.fxml"));
        contenedorPrincipal.setCenter(root);
    }

    public void menuItemCerrarSesion_action() throws IOException {
        Parent ventanaInicial = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        stage.setTitle("Sitema de envios");
        stage.setScene(new Scene(ventanaInicial,300,275));
        LoginController login = new LoginController();
        login.setStage(stage);
    }

    public void menuItemEliminar_action() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/eliminar-empleado.fxml"));
        contenedorPrincipal.setCenter(root);
    }

    public void menuItemEliminarEnvio_action() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/eliminar-envios.fxml"));
        contenedorPrincipal.setCenter(root);
    }

    public void menuItemAcercaDe_action(){
        ventanaTextoInformation("Grupo One. \nIntegrantes: \n- Anderson Valencia Bermúdez. \n- Wilmer Mario Leiva Esteban. \n- Wilson Villa. \nTécnicas de Programación.");
    }

    public void ventanaTextoInformation(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Acerca de");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
