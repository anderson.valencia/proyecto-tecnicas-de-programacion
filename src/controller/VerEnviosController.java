package controller;

import bsn.EnvioBsn;
import javafx.beans.Observable;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Envio;

import java.time.LocalDate;
import java.util.List;

public class VerEnviosController {

    @FXML
    private TableView<Envio> tblEnvios;

    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmId;
    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmDireccion;
    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmPrecio;
    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmCategoria;
    @FXML
    private TableColumn<Envio, Boolean> clmEntregado;

    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmFecha;

    EnvioBsn envioBsn;

    public VerEnviosController(){
        envioBsn = new EnvioBsn();
    }

    @FXML
    public void initialize(){

        clmId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmDireccion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDireccion()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecio()));
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFecha().toString()));
        clmCategoria.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategoria()));
        clmEntregado.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().isEntregado()));


        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();

        ObservableList<Envio> listaEnviosObservable = FXCollections.observableList(listaEnvios);

        tblEnvios.setItems(listaEnviosObservable);

    }

}
