package controller;

import bsn.EnvioBsn;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Empleado;
import model.Envio;

import java.util.ArrayList;
import java.util.List;

public class VerEnviosEmpleadoController {

    @FXML
    private TableView<Envio> tblEnvios;

    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmId;
    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmDireccion;
    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmPrecio;
    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmCategoria;
    @FXML
    private TableColumn<Envio, Boolean> clmEntregado;

    @FXML
    private javafx.scene.control.TableColumn<Envio, String> clmFecha;

    static Empleado empleado;
    EnvioBsn envioBsn;

    public void setEmpleado(Empleado empleado){
        this.empleado = empleado;
    }

    public VerEnviosEmpleadoController(){
        envioBsn = new EnvioBsn();
    }

    public void initialize(){

        clmId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmDireccion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDireccion()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecio()));
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFecha().toString()));
        clmCategoria.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategoria()));
        clmEntregado.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().isEntregado()));


        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        List<Envio> listaEnviosEmpleadoSeleccionado = new ArrayList<>();


        if(listaEnvios != null){
            for(Envio envio: listaEnvios){
                if(envio.getId().equals(empleado.getId())){
                    listaEnviosEmpleadoSeleccionado.add(envio);
                }
            }
        }


        ObservableList<Envio> listaEnviosObservable = FXCollections.observableList(listaEnviosEmpleadoSeleccionado);

        tblEnvios.setItems(listaEnviosObservable);
    }

}
