package controller;

import bsn.EmpleadoBsn;
import bsn.exception.EmpleadoYaExisteException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import model.Empleado;
import model.Envio;

import java.io.IOException;
import java.util.List;

public class EliminarEmpleadoController {

    @FXML
    ChoiceBox cBoxEmpleados;
    EliminarEnviosController eliminarEnviosController;
    EmpleadoBsn empleadoBsn;

    public EliminarEmpleadoController(){

        empleadoBsn = new EmpleadoBsn();
        eliminarEnviosController = new EliminarEnviosController();
    }

    @FXML
    public void initialize(){
        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();

        for(Empleado empleado:listaEmpleados){
            cBoxEmpleados.getItems().add(empleado.toString());
        }
    }

    public void btnEliminar_action() throws IOException, EmpleadoYaExisteException {
        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();
        String empleadoSeleccionado = cBoxEmpleados.getValue().toString();

        for(Empleado empleado:listaEmpleados){
            if(empleado.toString().equals(empleadoSeleccionado)){
                eliminarEnviosController.eliminarEnviosEmpleado(empleado);
                listaEmpleados.remove(empleado);
                ventanaTextoInformation("El empleado ha sido eliminado.");
                break;
            }
        }

        empleadoBsn.limpiar();
        for(Empleado empleado: listaEmpleados){
            empleadoBsn.registrarEmpleado(empleado);
        }


    }

    // Abrir una ventana información que de un mensaje determinado.
    public void ventanaTextoInformation(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Eliminar Empleado.");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    // Abrir una ventana de Error que de un mensaje determinado.
    public void ventanaTextoError(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Registro de empleado");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();

    }

}
