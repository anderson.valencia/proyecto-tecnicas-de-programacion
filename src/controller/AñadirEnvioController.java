package controller;

import bsn.EmpleadoBsn;
import bsn.EnvioBsn;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Empleado;
import model.Envio;

import javax.swing.event.ChangeListener;
import java.awt.image.ImagingOpException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.function.UnaryOperator;

public class AñadirEnvioController {

    @FXML
    ChoiceBox cBoxEmpleado, cBoxCategoria;

    @FXML
    TextField txtDireccion, txtPrecio, txtCategoria;

    @FXML
    DatePicker datePickerFecha;

    EnvioBsn envioBsn;

    private String idSeleccionado;

    public AñadirEnvioController(){
        this.envioBsn = new EnvioBsn();
    }

    @FXML
    public void initialize() {
        // Acceder al archivo de Empleados.
        EmpleadoBsn empleadoBsn;
        empleadoBsn = new EmpleadoBsn();
        List<Empleado> listaEmpleado = empleadoBsn.obtenerListaEmpleados();

        // Asignar los empledos al ChoiceBox.
        for (Empleado empleado : listaEmpleado) {
            cBoxEmpleado.getItems().add(empleado.getNombre() + "[" + empleado.getId() + "]");
        }

        // Obtener el id del empleado seleccionado.
        cBoxEmpleado.setOnAction(e -> {
            String empleadoSeleccionado = cBoxEmpleado.getSelectionModel().getSelectedItem().toString();
            for (Empleado empleado : listaEmpleado) {
                if (empleadoSeleccionado.equals(empleado.getNombre() + "[" + empleado.getId() + "]")) {
                    this.idSeleccionado = empleado.getId();
                }
            }
        });

        // Añadir elementos al ChoiceBox de Categoria.
        cBoxCategoria.getItems().add("Poco importante");
        cBoxCategoria.getItems().add("Normal");
        cBoxCategoria.getItems().add("Importante");


        UnaryOperator<TextFormatter.Change> filter = change -> {
            String text = change.getText();

            if (text.matches("[0-9]*")) {
                return change;
            }

            return null;
        };
        TextFormatter<String> textFormatter = new TextFormatter<>(filter);
        txtPrecio.setTextFormatter(textFormatter);
    }

    public void btnAñadir_action() {

        // Se extraen datos ingresados.
        String direccion = txtDireccion.getText();
        String precio = txtPrecio.getText();
        LocalDate fecha = datePickerFecha.getValue();
        String categoriaSeleccionada = cBoxCategoria.getValue().toString();

        // Validar campos nulos.
        if (cBoxEmpleado.getValue() == null) {
            ventanaTextoError("Por favor seleccione un empleado.");
            cBoxEmpleado.requestFocus();
            return;
        }
        if (txtDireccion.getText().isEmpty()) {
            ventanaTextoError("Por favor ingrese una dirección.");
            txtDireccion.requestFocus();
            return;
        }
        if (txtPrecio.getText().isEmpty()) {
            ventanaTextoError("Por favor ingrese el precio.");
            txtPrecio.requestFocus();
            return;
        }
        if (datePickerFecha.getValue() == null) {
            ventanaTextoError("Por favor seleccione la fecha.");
            datePickerFecha.requestFocus();
            return;
        }
        if (cBoxCategoria.getValue() == null) {
            ventanaTextoError("Por favor seleccione la categoria.");
            cBoxCategoria.requestFocus();
            return;
        }

        Envio envio = new Envio(idSeleccionado, direccion, precio, fecha,categoriaSeleccionada);
        envioBsn.añadirEnvio(envio);
        limpiarCampos();
        ventanaTextoInformation("El envio ha sido añadido.");




    }

    private void limpiarCampos() {
        this.txtDireccion.clear();
        this.txtPrecio.clear();
        this.datePickerFecha.setValue(null);
    }

    // Abrir una ventana información que de un mensaje determinado.
    public void ventanaTextoInformation(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Añadir envio");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    // Abrir una ventana de Error que de un mensaje determinado.
    public void ventanaTextoError(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Añadir Envio");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();

    }

}
