package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Empleado;

import java.io.IOException;

public class ContenedorEmpleadoController {

    static Empleado empleadoSeleccionado;

    VerEnviosEmpleadoController verEnviosEmpleadoController;
    GestionarEnviosEmpleadoController gestionarEnviosEmpleadoController;

    @FXML
    BorderPane contenedorEmpleado;

    static Stage stage;

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public ContenedorEmpleadoController(){
        verEnviosEmpleadoController = new VerEnviosEmpleadoController();
        gestionarEnviosEmpleadoController = new GestionarEnviosEmpleadoController();
    }

    public void setEmpleado(Empleado empleado){
        this.empleadoSeleccionado = empleado;

    }

    public void menuItemCerrar_action(){
        System.exit(0);
    }

    public void menuItemVerEnvios_action() throws IOException {

        verEnviosEmpleadoController.setEmpleado(empleadoSeleccionado);
        Parent root = FXMLLoader.load(getClass().getResource("../view/ver-envios-empleado.fxml"));
        contenedorEmpleado.setCenter(root);

    }

    public void menuItemGestionarEnvios_action() throws IOException {

        gestionarEnviosEmpleadoController.setEmpleado(empleadoSeleccionado);
        Parent root = FXMLLoader.load(getClass().getResource("../view/gestionar-envios-empleado.fxml"));
        contenedorEmpleado.setCenter(root);

    }

    public void menuItemCerrarSesion_action() throws IOException {

        Parent ventanaInicial = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        stage.setTitle("Sitema de envios");
        stage.setScene(new Scene(ventanaInicial,300,275));

        LoginController login = new LoginController();
        login.setStage(stage);
    }

}
