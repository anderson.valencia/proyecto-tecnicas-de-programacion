package controller;

import bsn.EmpleadoBsn;
import bsn.EnvioBsn;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import model.Empleado;
import model.Envio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GestionarEnviosController {

    @FXML
    ChoiceBox cBoxEmpleado, cBoxEnvio;

    @FXML
    Button btnEntregado, btnNoEntregado;

    EmpleadoBsn empleadoBsn;
    EnvioBsn envioBsn;

    public GestionarEnviosController() {
        empleadoBsn = new EmpleadoBsn();
        envioBsn = new EnvioBsn();
    }

    @FXML
    public void initialize() {
        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        for (Empleado empleado : listaEmpleados) {
            cBoxEmpleado.getItems().add(empleado.toString());
        }

    }



    public void btnBuscarEnvios_action(){

        cBoxEnvio.getItems().clear();

        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        Empleado empleadoNuevo = new Empleado();
        String idSeleccionado;

        String empleadoSeleccionado = cBoxEmpleado.getValue().toString();
        for (Empleado empleado : listaEmpleados) {
            if (empleado.toString().equals(empleadoSeleccionado)) {
                empleadoNuevo = empleado;
            }
        }

        idSeleccionado = empleadoNuevo.getId();

        List<Envio> listaEnviosEmpleadoSeleccionado = new ArrayList<>();

        for(Envio envio: listaEnvios){
            if(envio.getId().equals(idSeleccionado)){
                listaEnviosEmpleadoSeleccionado.add(envio);
            }
        }

        for(Envio envio: listaEnviosEmpleadoSeleccionado){
            cBoxEnvio.getItems().add(envio.toString());
        }
    }

    public void btnEntregado_action() throws IOException {
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        String envioSeleccionado = cBoxEnvio.getValue().toString();

        for(Envio envio: listaEnvios){
            if (envio.toString().equals(envioSeleccionado)) {
                envio.setEntregado(true);
            }
        }

        envioBsn.limpiar();
        for(Envio envio: listaEnvios){
            envioBsn.añadirEnvio(envio);
        }

    }

    public void btnNoEntregado_action() throws IOException {
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        String envioSeleccionado = cBoxEnvio.getValue().toString();

        for(Envio envio: listaEnvios){
            if (envio.toString().equals(envioSeleccionado)) {
                envio.setEntregado(false);
            }
        }

        envioBsn.limpiar();
        for(Envio envio: listaEnvios){
            envioBsn.añadirEnvio(envio);
        }
    }


}
