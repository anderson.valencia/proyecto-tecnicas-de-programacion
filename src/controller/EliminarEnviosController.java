package controller;

import bsn.EmpleadoBsn;
import bsn.EnvioBsn;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import model.Empleado;
import model.Envio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EliminarEnviosController {

    @FXML
    ChoiceBox cBoxEmpleado, cBoxEnvio;

    EmpleadoBsn empleadoBsn;
    EnvioBsn envioBsn;

    public EliminarEnviosController() {
        empleadoBsn = new EmpleadoBsn();
        envioBsn = new EnvioBsn();
    }

    @FXML
    public void initialize() {
        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        for (Empleado empleado : listaEmpleados) {
            cBoxEmpleado.getItems().add(empleado.toString());
        }

    }

    public void btnBuscarEnvios_action() {
        cBoxEnvio.getItems().clear();

        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        Empleado empleadoNuevo = new Empleado();
        String idSeleccionado;

        String empleadoSeleccionado = cBoxEmpleado.getValue().toString();
        for (Empleado empleado : listaEmpleados) {
            if (empleado.toString().equals(empleadoSeleccionado)) {
                empleadoNuevo = empleado;
            }
        }

        idSeleccionado = empleadoNuevo.getId();

        List<Envio> listaEnviosEmpleadoSeleccionado = new ArrayList<>();

        for (Envio envio : listaEnvios) {
            if (envio.getId().equals(idSeleccionado)) {
                listaEnviosEmpleadoSeleccionado.add(envio);
            }
        }

        for (Envio envio : listaEnviosEmpleadoSeleccionado) {
            cBoxEnvio.getItems().add(envio.toString());
        }
    }

    public void btnEliminar_action() throws IOException {
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        String envioSeleccionado = cBoxEnvio.getValue().toString();

        for (Envio envio : listaEnvios) {
            if (envio.toString().equals(envioSeleccionado)) {
                listaEnvios.remove(envio);
                ventanaTextoInformation("El envio ha sido eliminado.");
                break;
            }
        }

        envioBsn.limpiar();
        for (Envio envio : listaEnvios) {
            envioBsn.añadirEnvio(envio);
        }
    }

    public void eliminarEnviosEmpleado(Empleado empleado) throws IOException {
        List<Envio> listaEnvios = envioBsn.obtenerListaEnvios();
        List<Envio> listaEnviosEmpleado = new ArrayList<>();

        for(Envio envio: listaEnvios){
            if(envio.getId().equals(empleado.getId())){
                listaEnviosEmpleado.add(envio);
            }
        }
        if(listaEnviosEmpleado.isEmpty()){
            return;
        }


        boolean last = false;
        while (!last) {
            last = true;
            for (Envio envio : listaEnvios) {
                if (envio.getId().equals(empleado.getId())) {
                    listaEnvios.remove(envio);
                    break;
                }
            }
            for(Envio envio: listaEnvios){
                if(envio.getId().equals(empleado.getId())){
                    last = false;
                    break;
                }
            }
        }

            envioBsn.limpiar();
            if(!listaEnvios.isEmpty()) {
                for (Envio envio : listaEnvios) {
                    envioBsn.añadirEnvio(envio);
                }
            }

    }

    // Abrir una ventana información que de un mensaje determinado.
    public void ventanaTextoInformation(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Eliminar envio.");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    // Abrir una ventana de Error que de un mensaje determinado.
    public void ventanaTextoError(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Eliminar envio");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();

    }

}
