package controller;

import bsn.EmpleadoBsn;
import bsn.exception.EmpleadoYaExisteException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.Empleado;
import model.Envio;

import java.io.IOException;
import java.util.List;

public class RegistrarEmpleadoController {

    @FXML
    TextField txtNombre, txtId, txtCargo;

    EmpleadoBsn empleadoBsn;

    public RegistrarEmpleadoController(){
        this.empleadoBsn = new EmpleadoBsn();
    }

    public void btnRegistrar_action() {

        // Se extraen los datos ingresados en cada campo de texto.
        String nombreIngresado = txtNombre.getText();
        String idIngresado = txtId.getText();
        String cargoIngresado = txtCargo.getText();

        // validar campos nulos y su longitud.
        if (txtNombre.getText().trim().isEmpty()) {
            ventanaTextoError("Por favor ingrese el nombre del empleado.");
            txtNombre.requestFocus();
            return;
        }
        if (txtId.getText().trim().isEmpty()) {
            ventanaTextoError("Por favor ingrese el identificación del empleado.");
            txtId.requestFocus();
            return;
        }
        if (txtCargo.getText().trim().isEmpty()) {
            ventanaTextoError("Por favor ingrese el cargo del empleado.");
            txtCargo.requestFocus();
            return;
        }

        Empleado empleado = new Empleado(nombreIngresado, idIngresado, cargoIngresado);
        try {
            empleadoBsn.registrarEmpleado(empleado);
            ventanaTextoInformation("El empleado ha sido registrado con éxito.");
            limpiarCampos();
        } catch (EmpleadoYaExisteException e) {
            ventanaTextoError("Ya existe un empleado con el mismo id ingresado.");
            System.out.println(e.getMessage());

        }

    }

    private void limpiarCampos() {
        this.txtNombre.setText("");
        this.txtId.setText("");
        this.txtCargo.setText("");
    }

    // Abrir una ventana de información que de un mensaje determinado.
    public void ventanaTextoInformation(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Registro de empleado.");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    // Abrir una ventana de Error que de un mensaje determinado.
    public void ventanaTextoError(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Registro de empleado");
        alert.setHeaderText("");
        alert.setContentText(mensaje);
        alert.showAndWait();

    }

}
