package controller;

import bsn.EmpleadoBsn;
import bsn.exception.EmpleadoYaExisteException;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import model.Empleado;
import model.Envio;

import java.util.List;

public class VerEmpleadosController {

    @FXML
    private TableView<Empleado> tblEmpleados;

    @FXML
    private javafx.scene.control.TableColumn<Empleado, String> clmNombre;
    @FXML
    private javafx.scene.control.TableColumn<Empleado, String> clmId;
    @FXML
    private javafx.scene.control.TableColumn<Empleado, String> clmCargo;

    EmpleadoBsn empleadoBsn;

    public VerEmpleadosController(){
        empleadoBsn = new EmpleadoBsn();
    }

    @FXML
    public void initialize(){
        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));
        clmId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmCargo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCargo()));

        List<Empleado> listaEmpleados = empleadoBsn.obtenerListaEmpleados();

        ObservableList<Empleado> listaEmpleadosObservable = FXCollections.observableList(listaEmpleados);

        tblEmpleados.setItems(listaEmpleadosObservable);

    }

}
